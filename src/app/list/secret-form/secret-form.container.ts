import { Component, ChangeDetectionStrategy } from "@angular/core";
import { Store, select } from "@ngrx/store";

import * as fromRoot from "../../app.reducers";
import * as fromList from "../list.reducer";
import {
  TogglePasswordIsMasked,
  SetPasswordIsMasked,
  ToggleShowNotes,
  UpdateSecret,
  DeleteSecret,
  GeneratePassword,
  CreateSecret,
  AddGroupToSecret,
  RemoveGroupFromSecret
} from "./secret-form.actions";
import { HideCreate } from "../list.actions";
import { selectActiveGroups } from "../../data/group.selectors";

export const multiselectSecretText = {
  noSearchFieldMatchText: "None of your groups match this name.",
  noListItemsText: "You don't have any groups. Go to “Groups” to create one.",
  fewListItemsText:
    "You can add your password to any group on this list.",
  allListItemsSelectedText: "You have added your password to every group on this list.",
  justAddedText: `Once you press “Save Password”, this password will be added to |||.`
};

@Component({
  selector: "secret-form-container",
  template: `
    <secret-form-component
      [form]="form$ | async"
      [errorMessage]="errorMessage$ | async"
      [isNew]="isNew$ | async"
      [isUpdating]="isUpdating$ | async"
      [isUpdated]="isUpdated$ | async"
      [passwordIsMasked]="passwordIsMasked$ | async"
      [showNotes]="showNotes$ | async"
      [groups]="groups$ | async"
      [selectedGroups]="selectedGroups$ | async"
      [noSearchFieldMatchText]="noSearchFieldMatchText"
      [noListItemsText]="noListItemsText"
      [fewListItemsText]="fewListItemsText"
      [allListItemsSelectedText]="allListItemsSelectedText"
      [justAddedText]="justAddedText"
      [keyControls]="_keyControls"
      (toggleKeyControls)="toggleKeyControls()"
      (addGroupToSecret)="addGroupToSecret($event)"
      (removeGroupFromSecret)="removeGroupFromSecret($event)"
      (togglePasswordIsMasked)="togglePasswordIsMasked()"
      (setPasswordIsMasked)="setPasswordIsMasked($event)"
      (toggleShowNotes)="toggleShowNotes()"
      (saveSecret)="saveSecret($event)"
      (deleteSecret)="deleteSecret()"
      (generatePassword)="generatePassword()"
      (cancel)="cancel()"
    ></secret-form-component>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SecretFormContainer {
  form$ = this.store.pipe(select(fromList.getSecretForm));
  errorMessage$ = this.store.pipe(select(fromList.getSecretFormErrorMessage));
  isNew$ = this.store.pipe(select(fromList.getSecretIsNew));
  isUpdating$ = this.store.pipe(select(fromList.getSecretIsUpdating));
  isUpdated$ = this.store.pipe(select(fromList.getSecretIsUpdated));
  passwordIsMasked$ = this.store.pipe(
    select(fromList.getSecretPasswordIsMasked)
  );
  showNotes$ = this.store.pipe(select(fromList.getSecretShowNotes));
  groups$ = this.store.pipe(select(selectActiveGroups));
  selectedGroups$ = this.store.pipe(select(fromList.getSelectedGroups));

  // Multiselect variables
  noSearchFieldMatchText = multiselectSecretText.noSearchFieldMatchText;
  noListItemsText = multiselectSecretText.noListItemsText;
  fewListItemsText = multiselectSecretText.fewListItemsText;
  allListItemsSelectedText = multiselectSecretText.allListItemsSelectedText;
  justAddedText = multiselectSecretText.justAddedText;
  _keyControls = false;

  constructor(public store: Store<fromRoot.IState>) {}

  togglePasswordIsMasked() {
    this.store.dispatch(new TogglePasswordIsMasked());
  }

  setPasswordIsMasked(value: boolean) {
    this.store.dispatch(new SetPasswordIsMasked(value));
  }

  toggleShowNotes() {
    this.store.dispatch(new ToggleShowNotes());
  }

  saveSecret(create: boolean) {
    /**
     * If we're in the multiselect add widget, we don't want the Enter key to
     * submit this
     */
    if (!this._keyControls) {
      if (create) {
        this.store.dispatch(new CreateSecret());
      } else {
        this.store.dispatch(new UpdateSecret());
      }
    }
  }

  deleteSecret() {
    this.store.dispatch(new DeleteSecret());
  }

  generatePassword() {
    this.store.dispatch(new GeneratePassword());
  }

  cancel() {
    this.store.dispatch(new HideCreate());
  }

  addGroupToSecret(id: number) {
    this.store.dispatch(new AddGroupToSecret(id));
  }

  removeGroupFromSecret(id: number) {
    this.store.dispatch(new RemoveGroupFromSecret(id));
  }

  toggleKeyControls = () => (this._keyControls = !this._keyControls);
}
