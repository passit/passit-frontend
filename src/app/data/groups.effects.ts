import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { GroupService } from "../groups/group.service";
import { exhaustMap, map, catchError } from "rxjs/operators";
import { EMPTY } from "rxjs";
import { loadGroups, setGroups } from "./groups.actions";
import {
  saveGroupSuccess,
  deleteGroupSuccess
} from "../groups/groups-form/groups-form.actions";
import { initGroups } from "../groups/groups.actions";

@Injectable()
export class GroupEffects {
  loadGroup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadGroups, saveGroupSuccess, deleteGroupSuccess, initGroups),
      exhaustMap(() => {
        return this.groupService.getGroups().pipe(
          map(groups => setGroups({ groups })),
          catchError(() => EMPTY)
        );
      })
    )
  );

  constructor(private actions$: Actions, private groupService: GroupService) {}
}
