import {
  FormGroupState,
  createFormGroupState,
  validate,
  markAsSubmitted,
  updateGroup,
  createFormStateReducerWithUpdate,
  SetValueAction
} from "ngrx-forms";
import { required, pattern } from "ngrx-forms/validation";
import {
  createSelector,
  createFeatureSelector,
  ActionReducerMap
} from "@ngrx/store";
import { environment } from "../../environments/environment";

import { AppActions, AppActionTypes } from "../app.actions";
import { ILoginForm } from "./interfaces";
import { DEFAULT_API } from "../constants";
import { oldPasswordValidators } from "../account/constants";
import { IBaseFormState } from "../utils/interfaces";
import * as fromRoot from "../app.reducers";
import * as fromVerifyMfa from "./verify-mfa/verify-mfa.reducer";
import * as fromVerifyU2f from "./verify-mfa/verify-u2f/verify-u2f.reducer";

const FORM_ID = "Login Form";

export const validateAndUpdateFormState = updateGroup<ILoginForm>({
  email: validate(required, pattern(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/)),
  password: validate(oldPasswordValidators)
});

const initialFormState = validateAndUpdateFormState(
  createFormGroupState<ILoginForm>(FORM_ID, {
    email: "",
    password: "",
    rememberMe:
      environment.extension ? true : false,
    showUrl: false,
    url: environment.extension ? DEFAULT_API : ""
  })
);

export interface ILoginFormState extends IBaseFormState {
  form: FormGroupState<ILoginForm>;
  errorMessage: string | null;
}

export const initialLoginFormState: ILoginFormState = {
  form: initialFormState,
  hasStarted: false,
  hasFinished: false,
  errorMessage: null
};

export const formReducer = createFormStateReducerWithUpdate<ILoginForm>(
  validateAndUpdateFormState
);

export function loginFormReducer(
  state = initialLoginFormState,
  action: AppActions
): ILoginFormState {
  let form = formReducer(state.form, action);
  state = { ...state, form };

  switch (action.type) {
    case AppActionTypes.LOGIN_REDIRECT:
      if (action.payload) {
        const setValueAction = new SetValueAction(
          form.controls.email.id,
          action.payload
        );
        form = formReducer(state.form, setValueAction);
      }
      return {
        ...state,
        form
      };

    case AppActionTypes.LOGIN:
      return {
        ...state,
        form: markAsSubmitted(state.form),
        hasStarted: true,
        hasFinished: false,
        errorMessage: null
      };

    case AppActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        hasStarted: false,
        hasFinished: true
      };

    case AppActionTypes.LOGIN_FAILURE:
      return {
        ...state,
        hasStarted: false,
        errorMessage: action.payload
      };

    default:
      return state;
  }
}

interface ILoginState {
  loginForm: ILoginFormState;
  verifyMfa: fromVerifyMfa.IVerifyMfaState;
  verifyU2f: fromVerifyU2f.IVerifyU2fState;
}

export const reducers: ActionReducerMap<ILoginState> = {
  loginForm: loginFormReducer,
  verifyMfa: fromVerifyMfa.reducer,
  verifyU2f: fromVerifyU2f.reducer
};

export interface IState extends fromRoot.IState {
  login: ILoginState;
}

export const selectLoginState = createFeatureSelector<IState, ILoginState>(
  "login"
);

const selectLoginFormState = createSelector(
  selectLoginState,
  state => state.loginForm
);

const selectVerifyMfaState = createSelector(
  selectLoginState,
  state => state.verifyMfa
);

const selectVerifyU2fState = createSelector(
  selectLoginState,
  state => state.verifyU2f
);

export const getLoginHasStarted = createSelector(
  selectLoginFormState,
  state => state.hasStarted
);
export const getLoginHasFinished = createSelector(
  selectLoginFormState,
  state => state.hasFinished
);
export const getLoginErrorMessage = createSelector(
  selectLoginFormState,
  state => state.errorMessage
);
export const getLoginForm = createSelector(
  selectLoginFormState,
  state => state.form
);

export const selectVerifyMfaForm = createSelector(
  selectVerifyMfaState,
  state => state.form
);
export const getVerifyMfaHasStarted = createSelector(
  selectVerifyMfaState,
  state => state.hasStarted
);
export const getVerifyMfaFinished = createSelector(
  selectVerifyMfaState,
  state => state.hasFinished
);
export const getVerifyMfaErrorMessage = createSelector(
  selectVerifyMfaState,
  state => state.errorMessage
);

export const getSkipU2F = createSelector(
  selectVerifyMfaState,
  state => state.skipU2F
);

export const getU2fErrorMessage = createSelector(
  selectVerifyU2fState,
  state => state.errorMessage
);
