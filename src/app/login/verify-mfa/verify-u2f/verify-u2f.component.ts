import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input
  } from "@angular/core";

  @Component({
    selector: "app-verify-u2f",
    templateUrl: "./verify-u2f.component.html",
    styleUrls: ["./verify-u2f.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  export class VerifyU2fComponent {
    @Input() isPopup: boolean;
    @Input() isExtension: boolean;
    @Input() validAuth: string[];
    @Input() hasU2F: boolean;
    @Input() useU2F: boolean;
    @Input() u2fErrorMessage: string;

    @Output() goToLogin = new EventEmitter();
    @Output() switchMfaMethod = new EventEmitter();
    @Output() verifyU2f = new EventEmitter();

    ngOnInit() {
      if (this.useU2F && !this.isExtension) {
        this.verifyU2f.emit();
      }
    }
    constructor() {

    }
  }

