import { Action } from "@ngrx/store";

export enum VerifyMfaActionTypes {
  VERIFY_MFA = "[Verify MFA] Verify",
  VERIFY_MFA_SUCCESS = "[Verify MFA] Verify Success",
  VERIFY_MFA_FAILURE = "[Verify MFA] Verify Failure",
  SWITCH_MFA_METHOD = "[Login] Switch MFA Method"
}

export class VerifyMfa implements Action {
  readonly type = VerifyMfaActionTypes.VERIFY_MFA;
}

export class VerifyMfaSuccess implements Action {
  readonly type = VerifyMfaActionTypes.VERIFY_MFA_SUCCESS;
}

export class VerifyMfaFailure implements Action {
  readonly type = VerifyMfaActionTypes.VERIFY_MFA_FAILURE;
}

export class SwitchMfaMethod implements Action {
  readonly type = VerifyMfaActionTypes.SWITCH_MFA_METHOD;
}

export type VerifyMfaActions =
  | VerifyMfa
  | VerifyMfaSuccess
  | VerifyMfaFailure
  | SwitchMfaMethod;
