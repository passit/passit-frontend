import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { NgrxFormsModule } from "ngrx-forms";
import { InlineSVGModule } from "ng-inline-svg";
import { SelectModule } from "ng-select";

import { OrganizationsRoutingModule } from "./organizations-routing.module";
import { SharedModule } from "../shared/shared.module";
import { OrganizationEffects } from "./organizations.effects";
import { reducer } from "./organizations.reducers";
import { OrganizationsComponent } from "./organizations.component";
import { OrganizationsContainer } from "./organizations.container";
import { NewOrganizationComponent } from "./new-organization/new-organization.component";
import { OrganizationDetailComponent } from "./organization-detail/organization-detail.component";

export const COMPONENTS = [
  OrganizationsComponent,
  OrganizationsContainer,
  NewOrganizationComponent,
  OrganizationDetailComponent,
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OrganizationsRoutingModule,
    NgrxFormsModule,
    SelectModule,
    InlineSVGModule,
    StoreModule.forFeature("organizations", reducer),
    EffectsModule.forFeature([OrganizationEffects]),
  ],
  declarations: COMPONENTS,
})
export class OrganizationsModule {}
