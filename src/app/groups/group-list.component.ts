import { Component, EventEmitter, Input, Output } from "@angular/core";

import { IGroup } from "../../passit_sdk/api.interfaces";

@Component({
  selector: "group-list",
  styleUrls: ["../list/list.component.scss"],
  templateUrl: "./group-list.component.html"
})
export class GroupListComponent {
  groupAnimated = false;

  @Input() groups: IGroup[];
  @Input() groupManaged: number;

  @Output() groupSelected = new EventEmitter<IGroup>();

  onGroupWasSelected(group: IGroup) {
    this.groupAnimated = true;
    // I don't know why this doesn't work but similar code in list.component.ts does
    // if (this.groupManaged !== group.id) {
    //   setTimeout(() => {
    //     this.groupAnimated = true;
    //   }, 1);
    // }
    this.groupSelected.emit(group);
  }
}
