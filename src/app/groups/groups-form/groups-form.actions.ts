import { createAction, props } from "@ngrx/store";

export const saveGroup = createAction("[Groups Module] Save Group");
export const saveGroupSuccess = createAction(
  "[Groups Module] Save Group Success"
);
export const saveGroupFailure = createAction(
  "[Groups Module] Save Group Failure"
);
export const deleteGroup = createAction("[Groups Module] Delete Group");
export const deleteGroupSuccess = createAction(
  "[Groups Module] Delete Group Success"
);
export const deleteGroupFailure = createAction(
  "[Groups Module] Delete Group Failure"
);
export const contactLookup = createAction("[Groups Module] Contact Lookup");
export const contactLookupSuccess = createAction(
  "[Groups Module] Contact Lookup Success",
  props<{ label: string; value: number; disabled: boolean }>()
);
export const contactLookupFailure = createAction(
  "[Groups Module] Contact Lookup Failure"
);

export interface IAddListItem {
  userId: number;
  isPrivateOrgMode: boolean;
}
export const addUserToGroup = createAction(
  "[Groups Module] Add User To Group",
  props<IAddListItem>()
);

export const removeUserFromGroup = createAction(
  "[Groups Module] Remove User From Group",
  props<{ userId: number }>()
);
