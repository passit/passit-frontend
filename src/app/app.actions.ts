import { Action } from "@ngrx/store";
import { IAuthStore } from "./account/user/user.interfaces";

export enum AppActionTypes {
  LOGIN = "[Login] Login",
  LOGIN_SUCCESS = "[Login] Login Success",
  LOGIN_FAILURE = "[Login] Login Failure",
  LOGIN_REDIRECT = "[Login] Login Redirect",
}

export class LoginAction implements Action {
  readonly type = AppActionTypes.LOGIN;
}

export class LoginSuccessAction implements Action {
  readonly type = AppActionTypes.LOGIN_SUCCESS;

  constructor(public payload: IAuthStore) {}
}

export class LoginFailureAction implements Action {
  readonly type = AppActionTypes.LOGIN_FAILURE;

  constructor(public payload: string) {}
}

export class LoginRedirect implements Action {
  readonly type = AppActionTypes.LOGIN_REDIRECT;

  constructor(public payload?: string) {}
}

export type AppActions =
  | LoginAction
  | LoginSuccessAction
  | LoginFailureAction
  | LoginRedirect;
