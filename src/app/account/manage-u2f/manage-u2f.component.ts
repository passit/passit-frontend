import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroupState } from "ngrx-forms";
import { IU2fKey } from './manage-u2f.interfaces';
import { IEnableU2fForm } from "./manage-u2f.reducer";


@Component({
    selector: 'app-manage-u2f',
    templateUrl: './manage-u2f.component.html',
    styleUrls: [
        "./manage-u2f.component.scss",
        "../account.component.scss",
        "../../../styles/_utility.scss"
      ]
})

export class ManageU2fComponent {
    tooltipDisabled = false;

    @Input() isExtension: boolean;
    @Input() mfaRequired: boolean;
    @Input() u2fIsBegun: boolean;
    @Input() awaitingResponse: boolean;
    @Input() u2fErrors: string[] | null;
    @Input() u2fForm: FormGroupState<IEnableU2fForm>;
    @Input() regSuccess: boolean;
    @Input() u2fList: IU2fKey[];

    @Output() beginU2fReg = new EventEmitter();
    @Output() generateU2f = new EventEmitter();
    @Output() u2fReset = new EventEmitter();
    @Output() fetchU2fList = new EventEmitter();
    @Output() deleteU2fKey = new EventEmitter();

    ngOnInit() {
      this.fetchU2fList.emit();
    }

    ngOnDestroy() {
        this.u2fReset.emit();
    }

    checkIfTooltipIsNecessary($event: Event, maxWidth: number) {
      const target = $event.target as HTMLElement;
      const span =
        target.tagName === "DIV"
          ? (target.firstElementChild as HTMLElement)
          : target;
      const textWidth = span ? span.offsetWidth : -1;
      this.tooltipDisabled = textWidth >= maxWidth ? false : true;
    }

    constructor() {}
    }
