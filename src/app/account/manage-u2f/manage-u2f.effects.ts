import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { UserService } from "../user";
import {
  ManageU2fActionTypes,
  EnableU2fSuccess,
  EnableU2fFailure,
  FetchU2fListSuccess,
  FetchU2fListFailure,
  DeleteKey,
  DeleteKeySuccess,
  DeleteKeyFailure,
} from "./manage-u2f.actions";
import { of } from "rxjs";
import { Store, select } from "@ngrx/store";
import { IState } from "../../app.reducers";
import { exhaustMap, map, catchError, withLatestFrom, mergeMap } from "rxjs/operators";
import { getEnableU2fForm } from "../account.reducer";
import { IU2fKey } from "./manage-u2f.interfaces";


@Injectable()
export class ManageU2FEffects {
  @Effect()
  generateU2f$ = this.actions$.pipe(
    ofType(ManageU2fActionTypes.ENABLE_U2F),
    withLatestFrom(
      this.store.pipe(select(getEnableU2fForm))),
    exhaustMap(([action, form]) => {
      if (form.value.name) {
        return this.service.generateU2f(form.value.name).pipe(
          map(resp => {
            const key = <IU2fKey>resp;
            return new EnableU2fSuccess(key);
          }),
          catchError(resp => of(new EnableU2fFailure([resp])))
        );
      }
      return of(new EnableU2fFailure([]));
    })
  );

  @Effect()
  getU2fList$ = this.actions$.pipe(
    ofType(ManageU2fActionTypes.FETCH_U2F_LIST),
    exhaustMap(action =>
      this.service.getKeys().pipe(
        map(resp => resp.filter(key => key.key_type === "FIDO2")),
        map(resp => new FetchU2fListSuccess(resp)),
        catchError(resp => of(new FetchU2fListFailure(resp)))
      )
    )
  );

  @Effect()
  deleteU2fKey$ = this.actions$.pipe(
    ofType<DeleteKey>(ManageU2fActionTypes.DELETE_KEY),
    mergeMap(
      (data) => this.service.deleteU2fKey(data.payload)
        .pipe(
          map(() => new DeleteKeySuccess(data.payload)),
          catchError(error => of(new DeleteKeyFailure(error)))
        )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private service: UserService
  ) { }
}
