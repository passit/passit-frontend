import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { SetValueAction } from "ngrx-forms";
import * as fromAccount from "../../account.reducer";
import * as fromSetPassword from "./set-password.reducer";
import * as SetPasswordActions from "./set-password.actions";
import { LogoutSuccessAction } from "../../account.actions";
import { IState } from "../../../app.reducers";

@Component({
  template: `
    <app-set-password
      [form]="form$ | async"
      [hasStarted]="hasStarted$ | async"
      (toggleConfirm)="toggleConfirm()"
      (setPassword)="setPassword()"
      (backToLogin)="backToLogin()"
    ></app-set-password>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetPasswordContainer implements OnInit {
  form$ = this.store.pipe(select(fromAccount.getSetPasswordForm));
  hasStarted$ = this.store.pipe(select(fromAccount.getSetPasswordHasStarted));
  showConfirm: boolean;

  constructor(private store: Store<IState>) {
    this.form$.subscribe(
      form => (this.showConfirm = form.controls.showConfirm.value)
    );
  }

  setPassword() {
    this.store.dispatch(SetPasswordActions.setPassword());
  }

  toggleConfirm() {
    this.store.dispatch(
      new SetValueAction(
        fromSetPassword.FORM_ID + ".showConfirm",
        !this.showConfirm
      )
    );
  }

  backToLogin() {
    this.store.dispatch(new LogoutSuccessAction());
  }

  ngOnInit() {
    this.store.dispatch(SetPasswordActions.forceSetPassword());
  }
}
