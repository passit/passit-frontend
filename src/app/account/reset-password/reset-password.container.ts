import { Component, ChangeDetectionStrategy } from "@angular/core";
import * as fromAccount from "../account.reducer";
import { Store, select } from "@ngrx/store";
import { SubmitForm, ResetForm } from "./reset-password.actions";
import { IS_EXTENSION } from "../../constants";
import { IState } from "../../app.reducers";

@Component({
  template: `
    <app-reset-password
      [form]="form$ | async"
      [isExtension]="isExtension"
      [hasStarted]="hasStarted$ | async"
      [hasFinished]="hasFinished$ | async"
      [errorMessage]="errorMessage$ | async"
      [emailDisplayName]="emailDisplayName$ |async"
      (submitEmail)="submitEmail()"
      (reset)="reset()"
    ></app-reset-password>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResetPasswordContainer {
  emailDisplayName$ = this.store.pipe(select(fromAccount.getEmailDisplayName));
  form$ = this.store.pipe(select(fromAccount.getResetPasswordForm));
  hasStarted$ = this.store.pipe(select(fromAccount.getResetPasswordHasStarted));
  hasFinished$ = this.store.pipe(
    select(fromAccount.getResetPasswordHasFinished)
  );
  errorMessage$ = this.store.pipe(
    select(fromAccount.getResetPasswordErrorMessage)
  );
  isExtension = IS_EXTENSION;

  constructor(private store: Store<IState>) {}

  submitEmail() {
    this.store.dispatch(new SubmitForm());
  }

  reset() {
    this.store.dispatch(new ResetForm());
  }
}
