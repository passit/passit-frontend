import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { AbstractControlState, ValidationErrors } from "ngrx-forms";

@Component({
  selector: "app-password-input",
  templateUrl: "./password-input.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordInputComponent {
  @Input()
  showConfirm: AbstractControlState<boolean>;
  @Input()
  newPassword: AbstractControlState<string>;
  @Input()
  newPasswordConfirm: AbstractControlState<string>;
  @Input()
  errors: ValidationErrors;
  @Input()
  isSubmitted: boolean;
  @Output()
  toggleConfirm = new EventEmitter();

  constructor() {}
}
