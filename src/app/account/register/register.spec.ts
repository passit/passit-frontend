import { ComponentFixture, TestBed, fakeAsync, waitForAsync } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { EffectsModule } from "@ngrx/effects";
import { tick } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { StoreModule } from "@ngrx/store";
import { NgrxFormsModule } from "ngrx-forms";
import { InlineSVGModule } from "ng-inline-svg";
import { of } from "rxjs";
import { ProgressIndicatorModule } from "../../progress-indicator/progress-indicator.module";
import { RegisterComponent } from "./register.component";
import * as fromAccount from "../account.reducer";
import * as fromRoot from "../../app.reducers";
import { RegisterContainer } from "./register.container";
import { ConfirmEmailContainer, ConfirmEmailComponent } from "../confirm-email";
import { UserService } from "../user/user.service";
import { RegisterEffects } from "./register.effects";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { SharedModule } from "../../shared/shared.module";
import { BackupCodeComponent } from "../backup-code/backup-code.component";
import { Api } from "../../ngsdk/api";

/*
 * test setup
 */

describe("Register Component", () => {
  let component: RegisterContainer;
  let fixture: ComponentFixture<RegisterContainer>;
  let service: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        NgrxFormsModule,
        FormsModule,
        RouterTestingModule,
        InlineSVGModule.forRoot(),
        HttpClientTestingModule,
        ProgressIndicatorModule,
        SharedModule,
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("account", fromAccount.reducers),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([RegisterEffects]),
        NoopAnimationsModule
      ],
      declarations: [
        RegisterContainer,
        RegisterComponent,
        ConfirmEmailComponent,
        ConfirmEmailContainer,
        BackupCodeComponent
      ],
      providers: [
        { provide: NgPassitSDK },
        { provide: Api},
        {
          provide: UserService,
          useValue: jasmine.createSpyObj("UserService", [
            "checkUsername",
            "register"
          ])
        }
      ]
    }).compileComponents();
    service = TestBed.inject(UserService);
  }));

  function enterEmailInput(available: boolean) {
    const input = fixture.debugElement.query(By.css('input[type="email"]'));
    input.nativeElement.value = "test@example.com";
    input.nativeElement.dispatchEvent(new Event("input"));
    service.checkUsername.and.returnValue(of({ isAvailable: available }));
    tick();
    fixture.detectChanges();
    fixture.debugElement.nativeElement.querySelector("#email-button").click();
    tick();
  }

  function enterPasswordInput(value: string) {
    const input = fixture.debugElement.query(By.css('input[type="password"]'));
    input.nativeElement.value = value;
    input.nativeElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    fixture.debugElement.nativeElement.querySelector("#eye-icon").click();
    fixture.detectChanges();
    const IAuthStore = {
      publicKey: "abc",
      privateKey: "abc",
      userId: 1,
      email: "test@example.com",
      userToken: "abc",
      rememberMe: false
    };
    service.register.and.returnValue(of(IAuthStore));
    fixture.detectChanges();
    fixture.debugElement.nativeElement
      .querySelector("#password-button")
      .click();
    fixture.detectChanges();
  }

  function skipDownload() {
    fixture.detectChanges();
    fixture.debugElement.nativeElement.querySelector("#backup-skip").click();
  }

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should exist", () => {
    expect(component).toBeTruthy();
    fixture.destroy();
  });

  it("should display validation errors if email input is submitted empty", () => {
    fixture.debugElement.nativeElement.querySelector("#email-button").click();
    fixture.detectChanges();
    expect(
      fixture.nativeElement.innerText.indexOf(
        "Enter a valid email address."
      ) !== -1
    ).toBe(true);
    fixture.destroy();
  });

  it("should display validation errors if submitted email is not available", fakeAsync(async () => {
    enterEmailInput(false);
    await fixture.whenStable();
    fixture.detectChanges();
    expect(
      fixture.nativeElement.innerText.indexOf(
        "There is already an account registered with this email address."
      ) !== -1
    ).toBe(true);
    fixture.destroy();
  }));

  it("should display validation errors if password input is submitted empty", fakeAsync(async () => {
    enterEmailInput(true);
    await fixture.whenStable();
    fixture.detectChanges();
    enterPasswordInput("");
    expect(
      fixture.nativeElement.innerText.indexOf(
        "Enter a password for your account."
      ) !== -1
    ).toBe(true);
    tick();
    fixture.destroy();
  }));

  it("should display validation errors if password is only numbers", fakeAsync(async () => {
    enterEmailInput(true);
    await fixture.whenStable();
    fixture.detectChanges();
    enterPasswordInput("1111111111");
    expect(
      fixture.nativeElement.innerText.indexOf(
        "Password must not be only numbers."
      ) !== -1
    ).toBe(true);
    tick();
    fixture.destroy();
  }));

  it("should display validation errors if password is the same as email", fakeAsync(async () => {
    enterEmailInput(true);
    await fixture.whenStable();
    fixture.detectChanges();
    enterPasswordInput("test@example.com");
    expect(
      fixture.nativeElement.innerText.indexOf(
        "Password must not be email address."
      ) !== -1
    ).toBe(true);
    tick();
    fixture.destroy();
  }));

  it("should display password, and confirm code input if valid data is submitted", fakeAsync(async () => {
    enterEmailInput(true);
    await fixture.whenStable();
    fixture.detectChanges();
    expect(
      fixture.debugElement.query(By.css('input[type="password"]'))
    ).toBeTruthy();
    enterPasswordInput("helloworld");
    tick();
    await fixture.whenStable();
    skipDownload();
    await fixture.whenStable();
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css("#code"))).toBeTruthy();
    fixture.destroy();
  }));
});
