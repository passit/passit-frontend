import {
  FormGroupState,
  createFormGroupState,
  validate,
  markAsUnsubmitted,
  setValue,
  markAsSubmitted,
  updateGroup,
  createFormStateReducerWithUpdate,
  onNgrxForms,
  wrapReducerWithFormStateUpdate,
  onNgrxFormsAction,
  SetValueAction
} from "ngrx-forms";
import { createReducer, on, Action } from "@ngrx/store";

import { required, pattern, notEqualTo, equalTo } from "ngrx-forms/validation";
import { IRegisterForm, IUrlForm } from "./interfaces";
import { environment } from "../../../environments/environment";
import { RegisterStages, passwordValidators } from "../constants";
import { DEFAULT_API } from "../../constants";
import { verifyEmailSuccess } from "../confirm-email/confirm-email.actions";
import {
  register,
  registerSuccess,
  registerFailure,
  checkEmail,
  setIsEmailTaken,
  checkEmailSuccess,
  checkEmailFailure,
  checkUrl,
  checkUrlSuccess,
  displayUrl,
  hideUrl,
  checkUrlFailure,
  incrementStage,
  switchStage,
  registerClear
} from "./register.actions";

export const FORM_ID = "RegisterForm";
export const URL_FORM_ID = "UrlForm";

export const validateAndUpdateFormState = updateGroup<IRegisterForm>({
  email: validate(required, pattern(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/)),
  password: (password, form) => {
    return validate(password, [
      ...passwordValidators,
      notEqualTo(form.value.email)
    ]);
  },
  passwordConfirm: (passwordConfirm, form) => {
    if (form.controls.showConfirm.value) {
      return validate(passwordConfirm, [
        required,
        equalTo(form.value.password)
      ]);
    }
    return validate(passwordConfirm, []);
  }
});

const initialRegisterFormState = validateAndUpdateFormState(
  createFormGroupState<IRegisterForm>(FORM_ID, {
    email: "",
    password: "",
    passwordConfirm: "",
    showConfirm: true,
    signUpNewsletter: false,
    rememberMe: environment.extension ? true : false
  })
);

const initialUrlFormState = createFormGroupState<IUrlForm>(URL_FORM_ID, {
  url: DEFAULT_API
});

export interface IRegisterState {
  form: FormGroupState<IRegisterForm>;
  urlForm: FormGroupState<IUrlForm>;
  errorMessage: string | null;
  currentStage: RegisterStages;
  backupCode: string | null;
  isEmailTaken: boolean;
  isUrlValid: boolean | undefined;
  urlDisplayName: string;
  showUrl: boolean;
  hasSubmitStarted: boolean;
  hasSubmitFinished: boolean;
}

export const initialState: IRegisterState = {
  form: initialRegisterFormState,
  urlForm: initialUrlFormState,
  errorMessage: null,
  currentStage: RegisterStages.Email,
  backupCode: null,
  isEmailTaken: false,
  urlDisplayName: DEFAULT_API,
  isUrlValid: false,
  showUrl: false,
  hasSubmitStarted: false,
  hasSubmitFinished: false
};

const formReducer = createFormStateReducerWithUpdate<IRegisterForm>(
  validateAndUpdateFormState
);

export const urlValidateAndUpdateFormState = updateGroup<IUrlForm>({
  url: validate(required)
});
export const urlFormReducer = createFormStateReducerWithUpdate<IUrlForm>(
  urlValidateAndUpdateFormState
);

export const registerReducer = createReducer(
  initialState,
  onNgrxForms(),
  onNgrxFormsAction(SetValueAction, state => ({
    ...state,
    isEmailTaken: false
  })),
  on(register, state => ({
    ...state,
    hasSubmitStarted: true,
    hasSubmitFinished: false,
    errorMessage: null
  })),
  on(registerSuccess, (state, action) => ({
    ...state,
    hasSubmitStarted: false,
    hasSubmitFinished: true,
    currentStage: state.currentStage + 1,
    backupCode: action.payload.backupCode
  })),
  on(registerFailure, (state, action) => ({
    ...state,
    hasSubmitStarted: false,
    errorMessage: action.payload
  })),
  on(checkEmail, state => ({
    ...state,
    form: markAsSubmitted(state.form),
    hasSubmitStarted: true,
    backupCode: null
  })),
  on(setIsEmailTaken, (state, action) => ({
    ...state,
    hasSubmitStarted: false,
    isEmailTaken: action.payload
  })),
  on(checkEmailSuccess, state => ({
    ...state,
    form: markAsUnsubmitted(state.form),
    hasSubmitStarted: false,
    isEmailTaken: false,
    errorMessage: initialState.errorMessage,
    currentStage: state.currentStage + 1
  })),
  on(checkEmailFailure, (state, action) => ({
    ...state,
    errorMessage: action.payload,
    hasSubmitStarted: false
  })),
  on(checkUrl, state => ({
    ...state,
    urlForm: markAsSubmitted(state.urlForm)
  })),
  on(checkUrlSuccess, (state, action) => ({
    ...state,
    hasSubmitStarted: false,
    isUrlValid: true,
    showUrl: false,
    urlDisplayName: state.urlForm.value.url,
    currentStage: RegisterStages.Email,
    form: formReducer(
      setValue(initialState.form, { ...initialState.form.value }),
      action
    )
  })),
  on(displayUrl, state => ({
    ...state,
    showUrl: true
  })),
  on(hideUrl, state => ({
    ...state,
    showUrl: false
  })),
  on(checkUrlFailure, state => ({
    ...state,
    isUrlValid: false
  })),
  on(incrementStage, verifyEmailSuccess, state => ({
    ...state,
    currentStage: state.currentStage + 1
  })),
  on(switchStage, (state, action) => ({
    ...state,
    currentStage: action.payload,
    form: formReducer(
      setValue(initialState.form, {
        ...initialState.form.value,
        email: state.form.value.email,
        password:
          action.payload === RegisterStages.Password
            ? state.form.value.password
            : ""
      }),
      action
    )
  })),
  on(registerClear, () => ({ ...initialState }))
);

const wrappedReducer = wrapReducerWithFormStateUpdate(
  registerReducer,
  s => s.form,
  validateAndUpdateFormState
);

export function reducer(state: IRegisterState | undefined, action: Action) {
  return wrappedReducer(state, action);
}

export const getErrorMessage = (state: IRegisterState) => state.errorMessage;
export const getForm = (state: IRegisterState) => state.form;
export const getUrlForm = (state: IRegisterState) => state.urlForm;
export const getStage = (state: IRegisterState) => state.currentStage;
export const getBackupCode = (state: IRegisterState) => state.backupCode;
export const getUrlDisplayName = (state: IRegisterState) =>
  state.urlDisplayName;
export const getHasSubmitStarted = (state: IRegisterState) =>
  state.hasSubmitStarted;
export const getHasSubmitFinished = (state: IRegisterState) =>
  state.hasSubmitFinished;
export const getIsUrlValid = (state: IRegisterState) => state.isUrlValid;
export const getShowUrl = (state: IRegisterState) => state.showUrl;
export const getIsEmailTaken = (state: IRegisterState) => state.isEmailTaken;
