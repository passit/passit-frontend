import { Component } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { IState, getMfaRequired } from "../../app.reducers";
import {
  EnableMfa,
  ForwardStep,
  ActivateMfa,
  DeactivateMfa,
} from "./manage-mfa.actions";
import {
  getMfaProvisioningURI,
  getMfaStep,
  getEnableMfaForm,
  getMfaErrorMessage,
  getU2fErrorMessage,
  getEnableU2fForm,
  getU2fRegStatus,
  getWaitingStatus,
  getRegSuccess,
  getU2fList,
} from "../account.reducer";
import { BeginU2fReg, DeleteKey, EnableU2f, FetchU2fList, U2fReset } from "../manage-u2f/manage-u2f.actions";
import { IS_EXTENSION } from "../../constants";

@Component({
  template: `
    <app-manage-mfa
      [uri]="uri$ | async"
      [step]="step$ | async"
      [form]="form$ | async"
      [errors]="errors$ | async"
      [mfaRequired]="mfaRequired$ | async"
      [u2fList]="u2fList$ | async"
      (forwardStep)="forwardStep()"
      (generateMfa)="generateMfa()"
      (verifyMfa)="activateMfa()"
      (deactivateMfa)="deactivateMfa()"
    ></app-manage-mfa>
    <app-manage-u2f
      [isExtension]="isExtension"
      [u2fForm]="u2fForm$ | async"
      [mfaRequired]="mfaRequired$ | async"
      [u2fIsBegun]="u2fIsBegun$ | async"
      [u2fErrors]="u2fErrors$ | async"
      [awaitingResponse]="awaitingResponse$ | async"
      [regSuccess]="regSuccess$ | async"
      [u2fList]="u2fList$ | async"
      (beginU2fReg)="beginU2fReg()"
      (generateU2f)="generateU2f()"
      (u2fReset)="u2fReset()"
      (fetchU2fList)="fetchU2fList()"
      (deleteU2fKey)="deleteU2fKey($event)"
    ></app-manage-u2f>
  `
})
export class ManageMfaContainer {
  uri$ = this.store.pipe(select(getMfaProvisioningURI));
  step$ = this.store.pipe(select(getMfaStep));
  form$ = this.store.pipe(select(getEnableMfaForm));
  errors$ = this.store.pipe(select(getMfaErrorMessage));
  mfaRequired$ = this.store.pipe(select(getMfaRequired));

  u2fIsBegun$ = this.store.pipe(select(getU2fRegStatus));
  u2fForm$ = this.store.pipe(select(getEnableU2fForm));
  u2fErrors$ = this.store.pipe(select(getU2fErrorMessage));
  regSuccess$ = this.store.pipe(select(getRegSuccess));
  awaitingResponse$ = this.store.pipe(select(getWaitingStatus));
  u2fList$ = this.store.pipe(select(getU2fList));

  isExtension = IS_EXTENSION;

  constructor(private store: Store<IState>) {}

  forwardStep() {
    this.store.dispatch(new ForwardStep());
  }

  generateMfa() {
    this.store.dispatch(new EnableMfa());
  }

  activateMfa() {
    this.store.dispatch(new ActivateMfa());
  }

  deactivateMfa() {
    this.store.dispatch(new DeactivateMfa());
  }

  beginU2fReg() {
    this.store.dispatch(new BeginU2fReg());
  }

  generateU2f() {
    this.store.dispatch(new EnableU2f());
  }

  u2fReset() {
    this.store.dispatch(new U2fReset());
  }

  fetchU2fList() {
    this.store.dispatch(new FetchU2fList);
  }

  deleteU2fKey(id: number) {
    this.store.dispatch(new DeleteKey(id));
  }

}
