export enum ActivateMFAStep {
  Start = 0,
  DownloadApp = 1,
  ScanQR = 2,
  EnterCode = 3
}

export interface IGeneratedMFA {
  provisioning_uri: string;
  id: number;
}
