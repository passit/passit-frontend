import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType, Effect } from "@ngrx/effects";
import {
  switchMap,
  map,
  exhaustMap,
  tap,
  withLatestFrom
} from "rxjs/operators";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { Store, select } from "@ngrx/store";

import { UserService } from "../user";
import * as fromRoot from "../../app.reducers";
import {
  resetRegisterCode,
  resetRegisterCodeSuccess,
  resetRegisterCodeFailure,
  verifyEmail,
  verifyEmailSuccess,
  verifyEmailFailure
} from "./confirm-email.actions";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { getRouterPath } from "../../app.reducers";

@Injectable()
export class ConfirmEmailEffects {
  ResetRegisterCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(resetRegisterCode),
      switchMap(() => {
        return this.userService
          .resetRegisterCode()
          .then((resp: any) => resetRegisterCodeSuccess())
          .catch((error: any) => resetRegisterCodeFailure());
      })
    )
  );

  @Effect()
  VerifyEmail$ = this.actions$.pipe(
    ofType<any>(verifyEmail.type),
    map(action => action.payload),
    exhaustMap(code =>
      this.sdk
        .confirm_email_short_code(code)
        .then(() => verifyEmailSuccess())
        .catch(err => {
          const res: HttpErrorResponse = err.res;
          const status = res.status;
          let errorMessage = "Unexpected error.";
          if (status === 404 || status === 400) {
            errorMessage =
              "This code doesn't match what we sent. Please try again.";
          } else if (status === 406) {
            errorMessage = `We invalidated this code because of too many failed attempts.
            Press the "Resend confirmation" link below to receive a new code.`;
          } else if (status === 403) {
            return (errorMessage =
              "To validate via confirmation code, please log in first.");
          }
          return verifyEmailFailure({ payload: errorMessage });
        })
    )
  );

  VerifyEmailSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(verifyEmailSuccess),
        withLatestFrom(this.store.pipe(select(getRouterPath))),
        map(([action, path]) => path),
        tap(path => {
          // The confirm email page automatically redirects while register does not
          if (path === "/confirm-email") {
            this.router.navigate(["/list"]);
          }
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private sdk: NgPassitSDK,
    private router: Router,
    private store: Store<fromRoot.IState>
  ) {}
}
