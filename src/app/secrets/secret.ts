/* tslint:disable:variable-name */
import * as api from "../../passit_sdk/api.interfaces";
import * as sdk from "../../passit_sdk/sdk.interfaces";

/**
 * Secret model. Constructor accepts both ISecret and IDBSecret
 * and transforms these simple data types into a complex object with
 * handy functions like get password
 */
export class Secret implements sdk.INewSecret {
  public id?: number;
  public name?: string;
  public type?: string;
  public secrets?: api.IData;
  public group_id?: any;
  public group_name?: any;
  public dbSecret?: api.ISecret;
  public visible_data?: api.IData;

  /** Pass existing secret like object, if not is provided
   * a new secret like object is created.
   */
  constructor(public polySecret?: api.ISecret | sdk.ISecret) {
    // These match both types so let's set in any case
    if (polySecret) {
      this.id = polySecret.id;
      this.name = polySecret.name;
      this.type = polySecret.type;

      // Determine type
      if ((polySecret as api.ISecret).secret_through_set) {
        const dbSecret = polySecret as api.ISecret;
        this.secrets = dbSecret.secret_through_set as any;
        this.visible_data = dbSecret.data;
        this.dbSecret = dbSecret;
      } else {
        const secret = polySecret as sdk.ISecret;
        this.visible_data = secret.visible_data;
        this.secrets = secret.secrets;
      }
    } else {
      // Initialize new object with expected data fields
      this.visible_data = {};
      this.secrets = {};
    }
  }
}
